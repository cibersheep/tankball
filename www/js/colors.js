KTB.Colors={
    "gameboard":"#F6F6F5",
    "gameboard_border":"#AEA79F",

    "ball_normal":"#F79A2C",
    "ball_growing":"#F79A2C",
    "ball_stopped":"#E95420",
    "score":"#333333",
    "hiscore":"#335280",
    "ball_number":"#111",
    "tank_line":"#111",

    "item":"#111",
    "preview_border":"#333",
    "tank":"#000",
};
